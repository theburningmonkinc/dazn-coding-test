'use strict';

module.exports = {
  USER_TABLE_NAME: 'yancui-dazn-users',
  STREAM_TABLE_NAME: 'yancui-dazn-streams',
  USER_STREAM_TABLE_NAME: 'yancui-dazn-user-streams'
};