'use strict';

const Promise     = require('bluebird');
const co          = require('co');
const AWSXRay     = require('aws-xray-sdk');
const AWS         = AWSXRay.captureAWS(require('aws-sdk'));
AWS.config.region = 'eu-west-1';
const dynamodb    = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
const constants   = require('./constants');
const tableName   = constants.USER_STREAM_TABLE_NAME;
const errors      = require('./errors');
const log         = require('./log');

const activate = co.wrap(function* (userId, streamId) {
  const streamIdAsSet = dynamodb.createSet(streamId);
  const req = {
    TableName: tableName,
    Key: { userId },
    ConditionExpression: 'attribute_not_exists(userId) OR size(streams) < :max_size OR contains(streams, :streamId)',
    UpdateExpression: 'ADD streams :streamIdAsSet',
    ExpressionAttributeValues: {
      ':max_size'      : 3,
      ':streamId'      : streamId,
      ':streamIdAsSet' : streamIdAsSet
    }
  };
  
  try {
    yield dynamodb.updateAsync(req);
    log.debug(`added [${streamId}] to user [${userId}]'s live streams`);
  } catch (err) {
    if (err.code === 'ConditionalCheckFailedException') {
      throw new errors.MaxActivatedStreamsReached(userId);
    } else {
      throw new errors.DynamoDBConditionalUpdateFailed(err);
    }
  }
});

const deactivate = co.wrap(function* (userId, streamId) {
  const streamIdAsSet = dynamodb.createSet(streamId);
  const req = {
    TableName: tableName,
    Key: { userId },
    ConditionExpression: 'attribute_exists(userId) AND contains(streams, :streamId)',
    UpdateExpression: 'DELETE streams :streamIdAsSet',
    ExpressionAttributeValues: {
      ':streamId'      : streamId,
      ':streamIdAsSet' : streamIdAsSet
    }
  };

  try {
    yield dynamodb.updateAsync(req);
    log.debug(`removed [${streamId}] from user [${userId}]'s live streams`);
  } catch (err) {
    if (err.code === 'ConditionalCheckFailedException') {
      // conditional check failed = stream doesn't exist, so net result = what we
      // wanted, i.e. stream is removed from user's list
      return; 
    } else {
      throw new errors.DynamoDBConditionalUpdateFailed(err);
    }
  }
});

module.exports = {
  activate,
  deactivate
}