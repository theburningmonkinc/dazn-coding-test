'use strict';

const errorCodes = require('./errorCodes');

function AppError() {  
}
AppError.prototype = new Error();

function MaxActivatedStreamsReached(userId) {
  this.name = 'MaxActivatedStreamsReached';
  this.message = "I'm afraid you can't activate any more streams. Please deactivate another stream and try again."
  this.internalMessage = `user [${userId}] have reached the max no. of activated streams`;
  this.errorCode = errorCodes.MAX_ACTIVATED_STREAM_REACHED;
  this.statusCode = 400;
}
MaxActivatedStreamsReached.prototype = new AppError();

function DynamoDBConditionalUpdateFailed(tableName, caught) {
  this.name = 'DynamoDBConditionalUpdateFailed';
  this.message = "Oops, looks like we're experiencing some technical issues. Please try again later."
  this.internalMessage = `Conditional Update against DynamoDB table [${tableName}] failed: ${caught}`;
  this.errorCode = errorCodes.DDB_COND_UPDATE_FAILED;
  this.statusCode = 500;
}
MaxActivatedStreamsReached.prototype = new AppError();

module.exports = {
  AppError,
  MaxActivatedStreamsReached,
  DynamoDBConditionalUpdateFailed
};