'use strict';

// 11000 - 11999 = user fault
// 12000 - 12999 = issue with dependency
module.exports = {
  MAX_ACTIVATED_STREAM_REACHED: 11000,
  DDB_COND_UPDATE_FAILED: 12000
};