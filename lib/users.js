'use strict';

const _           = require('lodash');
const Promise     = require('bluebird');
const co          = require('co');
const AWSXRay     = require('aws-xray-sdk');
const AWS         = AWSXRay.captureAWS(require('aws-sdk'));
AWS.config.region = 'eu-west-1';
const dynamodb    = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
const constants   = require('./constants');
const tableName   = constants.USER_TABLE_NAME;
const httpError   = require('http-errors');

const must_exist = co.wrap(function* (userId) {
  const req = {
    TableName : tableName,
    Key: { userId }
  };

  const resp = yield dynamodb.getAsync(req);  
  if (!resp.Item || _.isEmpty(resp)) {
    throw new httpError.NotFound();
  }
});

module.exports = {
  must_exist
};