'use strict';

const { HttpError } = require('http-errors');
const { AppError }  = require('../errors');
const log           = require('../log');

// see https://github.com/middyjs/middy#writing-a-middleware for details
module.exports = {
  onError: (handler, next) => {
    console.error(JSON.stringify(handler.event));

    if (handler.error instanceof HttpError) {
      log.error(handler.error.message);

      handler.response = {
        statusCode: handler.error.statusCode,
        body: JSON.stringify({
          errorCode: -1,
          errorMessage: handler.error.message
        })
      };

      return next();
    }

    if (handler.error instanceof AppError) {
      const appError = handler.error;

      log.error(appError.internalMessage);

      handler.response = {
        statusCode: appError.statusCode,
        body: JSON.stringify({
          errorCode: appError.errorCode,
          errorMessage: appError.message
        })
      };

      return next();
    }

    return next(handler.error);
  }
};