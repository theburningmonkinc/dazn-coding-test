'use strict';

const log        = require('../log');
const reqContext = require('../requestContext');

function setRequestContext (event, context) {
  if (!event.headers) {
    log.warn(`Request ${context.awsRequestId} is missing headers`);
    return;
  } 

  let ctx = { awsRequestId : context.awsRequestId };
  for (var header in event.headers) {
    if (header.toLowerCase().startsWith("x-correlation-")) {
      ctx[header] = event.headers[header]
    }
  }
 
  if (!ctx["x-correlation-id"]) {
    ctx["x-correlation-id"] = ctx.awsRequestId;
  }

  if (event.headers["User-Agent"]) {
    ctx["User-Agent"] = event.headers["User-Agent"]; 
  }

  if (event.headers["Debug-Log-Enabled"] === 'true') {
    ctx["Debug-Log-Enabled"] = "true"
  }

  reqContext.replaceAllWith(ctx);
};

// see https://github.com/middyjs/middy#writing-a-middleware for details
module.exports = {
  before: (handler, next) => {
    reqContext.clearAll();

    try {
      setRequestContext(handler.event, handler.context);
    } catch (err) {
      log.warn(`couldn't set current request context: ${err}`, err.stack);
    }

    next();
  }
};