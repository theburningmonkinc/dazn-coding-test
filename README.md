# DAZN coding challenge

From a high-level, the implementation looks like this:

![](architecture.png)

Essentially you have an REST API (implemented with `API Gateway` and `AWS Lambda`) where the data is backed by several `DynamoDB` tables.

**note**: I didn't end up implementing the browser side of things, but I imagined it'll be a SPA app loaded via CloudFront.

## frameworks

* [Serverless framework](https://serverless.com/) for deployment and resource management, I explained in the [tech_decisions doc](tech_decisions.md) why I chose the Serverless framework and why I'm managing the DynamoDB tables with it
* [middy](https://github.com/middyjs/middy) middleware engine for AWS Lambda that allows me to capture common, cross-cutting functionalities as middleware

plugins for serverless framework:

* [serverless-iam-roles-per-function](https://github.com/functionalone/serverless-iam-roles-per-function) : enables per-function IAM policies to better apply the `principle of least privilege`
* [serverless-plugin-tracing](https://github.com/alex-murashkin/serverless-plugin-tracing) : enables tracing integration with X-Ray
* [serverless-pseudo-parameters](https://github.com/svdgraaf/serverless-pseudo-parameters) : makes referencing the current AWS region and account ID in the `serverless.yml` so much easier
* [serverless-plugin-warmup](https://github.com/FidelLimited/serverless-plugin-warmup) : enables prewarming of functions to mitigate effects of cold start, however, as I explained [here](https://hackernoon.com/im-afraid-you-re-thinking-about-aws-lambda-cold-starts-all-wrong-7d907f278a4f) this practice is not effective even under moderate load, but it's good enough for this coding test

## getting started

First, run `./build.sh deploy dev` using an AWS profile with admin access to the AWS account, this will create all the resources required to run the API:

* Lambda functions
* API Gateway
* DynamoDB tables

(I explained why I included DynamoDB tables as additional resource in the [tech_decisions doc](tech_decisions.md))

Take a note of the console output, and write down the root URL for the deployed API Gateway endpoints, you'll need this to run the end-to-end acceptane tests later.

![](screenshots/deployment-screenshot.png)

To seed some data into the DynamoDB tables, run `./build.sh seed`, this generates 10 random users & 10 random streams for you to test the API with.

To run the **integration tests** (running the functions locally, but talking to the real DynamoDB tables), run `./build.sh int-test`.

![](screenshots/int-test-screenshot.png)

To run the end-to-end **acceptance tests** (curling the remote, deployed API), run `./build.sh acceptance-test [put the URL you copied earlier]`. For me, it'll be `./build.sh acceptance-test https://yph44bi54e.execute-api.eu-west-1.amazonaws.com/dev`.

This would execute the same test cases as the integration tests, except, where the integration tests would invoke the functions locally with stubbed invocation event cand context, these end-to-end tests would interact with the API via HTTP.

![](screenshots/acceptance-test-screenshot.png)

Once you're done with reviewing this submission, run `./build.sh teardown dev`.

This'll remove **ALL** of the resources you have created earlier, the API Gateway API, the Lambda functions, as well as all the DynamoDB tables.

All the technical decisions that I thought were worth discussing have been documented in the [tech_decisions doc](tech_decisions.md).

## DynamoDB tables

* `yancui-dazn-users` : where the details of the users are kept
* `yancui-dazn-streams` : where details of the streams are kept
* `yancui-dazn-user-streams` : this table records what streams a user has activated

## Logging

All the logs from the Lambda functions are in CloudWatch Logs, and you can access it via the Lambda management console (click on the `Monitoring` tab).

The functions use structure logging with JSON and you get some additional system messages from the Lambda service. Most people find CloudWatch Logs is not a good enough log aggregation service, and fortunately it's fairly straight forward to [get your logs out of CloudWatch Logs](https://hackernoon.com/centralised-logging-for-aws-lambda-b765b7ca9152).

![](screenshots/cw-logs-screenshot.png)

## Tracing

Distributed tracing is done via X-Ray, I haven't done much instrumentation, only what you get out-of-the-box plus wrapping the AWS SDK calls (i.e. the DynamoDB requests).

Once you run the acceptance tests, these traces would be available in the X-Ray dashboard.

![](screenshots/x-ray-screenshot-1.png)
![](screenshots/x-ray-screenshot-2.png)
![](screenshots/x-ray-screenshot-3.png)

Additionally, the Lambda functions are capturing correlation IDs through the incoming HTTP headers already (as described by this [post](https://hackernoon.com/capture-and-forward-correlation-ids-through-different-lambda-event-sources-220c227c65f5)) and including them in all the log messages.

When we have outgoing requests to other APIs/SNS/Kinesis then we can easily pass those correlation IDs along to make it easy to find all the relevant logs from all the functions on the same call chain.