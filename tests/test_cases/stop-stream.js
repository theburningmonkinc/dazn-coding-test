'use strict';

const _        = require('lodash');
const co       = require('co');
const expect   = require('chai').expect;
const given    = require('../steps/given');
const tearDown = require('../steps/tearDown');
const when     = require('../steps/when');

describe(`Given a user has an active stream`, () => {
  let user, stream;

  before(co.wrap(function* () {
    user   = yield given.a_random_user();
    stream = yield given.a_random_stream();
    yield when.user_starts_stream(user.userId, stream.streamId);
  }));

  after(co.wrap(function* () {
    yield tearDown.a_user(user);
    yield tearDown.a_stream(stream);
    yield tearDown.all_activated_streams(user);
  }));

  describe(`When he tries to deactivate it`, () => {
    let resp;   

    before(co.wrap(function* () {
      resp = yield when.user_stops_stream(user.userId, stream.streamId);
    }));

    it(`Should return 200`, () => {
      expect(resp.statusCode).to.equal(200);
    });
  });

  describe(`And if he tries to deactivate it again`, () => {
    let resp;   

    before(co.wrap(function* () {
      resp = yield when.user_stops_stream(user.userId, stream.streamId);
    }));

    it(`Should still return 200`, () => {
      expect(resp.statusCode).to.equal(200);
    });
  });
});

describe(`Given {userId} path parameter is missing`, () => {
  let resp;

  before(co.wrap(function* () {
    resp = yield when.user_stops_stream("", "abc");
  }));

  it(`Should return 400`, () => {
    expect(resp.statusCode).to.equal(400);
  });
});