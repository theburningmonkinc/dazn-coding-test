'use strict';

const _          = require('lodash');
const co         = require('co');
const expect     = require('chai').expect;
const given      = require('../steps/given');
const tearDown   = require('../steps/tearDown');
const when       = require('../steps/when');
const errorCodes = require('../../lib/errorCodes');

describe(`Given a user don't have any active streams`, () => {
  let user, stream;

  before(co.wrap(function* () {
    user   = yield given.a_random_user();
    stream = yield given.a_random_stream();
  }));

  after(co.wrap(function* () {
    yield tearDown.a_user(user);
    yield tearDown.a_stream(stream);
    yield tearDown.all_activated_streams(user);
  }));

  describe(`When he tries to watch one`, () => {
    let resp;   

    before(co.wrap(function* () {
      resp = yield when.user_starts_stream(user.userId, stream.streamId);    
    }));

    it(`Should return 200`, () => {
      expect(resp.statusCode).to.equal(200);
    });
  });
});

describe(`Given a user who have 2 active streams already`, () => {
  let user, streams;

  before(co.wrap(function* () {
    user    = yield given.a_random_user();
    streams = yield _.range(0, 3).map(given.a_random_stream);

    yield when.user_starts_stream(user.userId, streams[0].streamId);
    yield when.user_starts_stream(user.userId, streams[1].streamId);
  }));

  after(co.wrap(function* () {
    yield tearDown.a_user(user);
    yield tearDown.many_streams(streams);
    yield tearDown.all_activated_streams(user);
  }));

  describe(`When he tries to watch one more`, () => {
    let resp;

    before(co.wrap(function* () {
      resp = yield when.user_starts_stream(user.userId, streams[2].streamId);
    }));

    it(`Should return 200`, () => {
      expect(resp.statusCode).to.equal(200);
    });
  });
});

describe(`Given a user who already have 3 active streams already`, () => {
  let user, streams;

  before(co.wrap(function* () {
    user    = yield given.a_random_user();
    streams = yield _.range(0, 4).map(given.a_random_stream);

    yield when.user_starts_stream(user.userId, streams[0].streamId);
    yield when.user_starts_stream(user.userId, streams[1].streamId);
    yield when.user_starts_stream(user.userId, streams[2].streamId);
  }));

  after(co.wrap(function* () {
    yield tearDown.a_user(user);
    yield tearDown.many_streams(streams);
    yield tearDown.all_activated_streams(user);
  }));

  describe(`When he tries to watch another new stream`, () => {
    let resp;

    before(co.wrap(function* () {
      resp = yield when.user_starts_stream(user.userId, streams[3].streamId);
    }));

    it(`Should return 400`, () => {
      expect(resp.statusCode).to.equal(400);
      expect(resp.body).to.not.be.null;
      expect(resp.body.errorCode).to.equal(errorCodes.MAX_ACTIVATED_STREAM_REACHED);
    });
  });

  describe(`When he tries to re-watch an existing stream`, () => {
    let resp;

    before(co.wrap(function* () {
      resp = yield when.user_starts_stream(user.userId, streams[2].streamId);
    }));

    it(`Should return 200`, () => {
      expect(resp.statusCode).to.equal(200);
    });
  });

});

describe(`Given {userId} path parameter is missing`, () => {
  let resp;   

  before(co.wrap(function* () {
    resp = yield when.user_starts_stream("", "abc");
  }));

  it(`Should return 400`, () => {
    expect(resp.statusCode).to.equal(400);
  });
});

describe(`Given the request user doesn't exist`, () => {
  let resp;   

  before(co.wrap(function* () {
    resp = yield when.user_starts_stream("fake-user", "fake-stream");
  }));

  it(`Should return 404`, () => {
    expect(resp.statusCode).to.equal(404);
  });
});

describe(`Given the request stream doesn't exist`, () => {
  let user, resp;   

  before(co.wrap(function* () {
    user = yield given.a_random_user();
    resp = yield when.user_starts_stream(user.userId, "fake-stream");
  }));

  after(co.wrap(function* () {
    yield tearDown.a_user(user);
  }));

  it(`Should return 404`, () => {
    expect(resp.statusCode).to.equal(404);
  });
});