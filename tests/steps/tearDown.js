'use strict';

const _           = require('lodash');
const co          = require('co');
const Promise     = require('bluebird');
const AWS         = require('aws-sdk');
AWS.config.region = 'eu-west-1';
const dynamodb    = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
const constants   = require('../../lib/constants');
const log         = require('../../lib/log');

const userTable        = constants.USER_TABLE_NAME;
const streamTable      = constants.STREAM_TABLE_NAME;
const userStreamsTable = constants.USER_STREAM_TABLE_NAME;

const a_user = co.wrap(function* (user) {
  const userId = user.userId;
  const req = {
    TableName : userTable,
    Key: { userId }
  };
  yield dynamodb.deleteAsync(req);

  log.debug(`deleted user [${userId}]`);
});

const a_stream = co.wrap(function* (stream) {
  const streamId = stream.streamId;
  const req = {
    TableName : streamTable,
    Key: { streamId }
  };
  yield dynamodb.deleteAsync(req);

  log.debug(`deleted stream [${streamId}]`);
});

const many_streams = co.wrap(function* (streams) {
  yield _.map(streams, a_stream);
});

const all_activated_streams = co.wrap(function* (user) {
  const userId = user.userId;
  const req = {
    TableName : userStreamsTable,
    Key: { userId }
  };
  yield dynamodb.deleteAsync(req);

  log.debug(`deleted all activated streams for [${userId}]`);
});

module.exports = {
  a_user,
  a_stream,
  many_streams,
  all_activated_streams
};