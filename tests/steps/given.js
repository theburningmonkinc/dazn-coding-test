'use strict';

const co          = require('co');
const Promise     = require('bluebird');
const AWS         = require('aws-sdk');
AWS.config.region = 'eu-west-1';
const dynamodb    = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
const chance      = require('chance').Chance();
const log         = require('../../lib/log');
const constants   = require('../../lib/constants');
const userTable   = constants.USER_TABLE_NAME;
const streamTable = constants.STREAM_TABLE_NAME;

const a_random_user = co.wrap(function* () {
  const userId    = chance.guid();
  const firstName = chance.first();
  const lastName  = chance.last();
  const email     = chance.email();
  
  const user = { userId, firstName, lastName, email };

  const req = {
    TableName : userTable,
    Item: user
  };
  yield dynamodb.putAsync(req);

  log.debug(`created user [${userId}]: ${firstName} ${lastName}`);

  return user;
});

const a_random_stream = co.wrap(function* () {
  const streamId = chance.guid();
  const title    = chance.sentence({ words: 5 });

  const stream = { streamId, title };

  const req = {
    TableName : streamTable,
    Item: stream
  };
  yield dynamodb.putAsync(req);

  log.debug(`created stream [${streamId}]: ${title}`);

  return stream;
});

module.exports = {
  a_random_user,
  a_random_stream
};