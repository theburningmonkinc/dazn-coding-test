'use strict';

const _       = require('lodash');
const co      = require('co');
const Promise = require("bluebird");
const http    = require('superagent-promise')(require('superagent'), Promise);
const log     = require('../../lib/log');
const mode    = process.env.TEST_MODE;

function respondFrom(httpRes) {
  const contentType = _.get(httpRes, 'headers.content-type', 'application/json');
  const body = 
    contentType === 'application/json'
      ? httpRes.body
      : httpRes.text;

  return { 
    statusCode: httpRes.status,
    body: body,
    headers: httpRes.headers
  };
}

const viaHttp = co.wrap(function* (relPath, method, opts) {
  const root = process.env.TEST_ROOT;
  const url = `${root}/${relPath}`;
  log.debug(`invoking via HTTP ${method} ${url}`);

  try {
    let httpReq = http(method, url);

    const body = _.get(opts, 'body');
    if (body) {      
      httpReq.send(body);
    }

    const headers = _.get(opts, 'headers', {});
    _.forEach(headers, (value, key) => {
      httpReq.set(key, value);
    });
    
    const res = yield httpReq;
    return respondFrom(res);
  } catch (err) {
    if (err.status) {
      return respondFrom(err.response);
    } else {
      throw err;
    }
  }
})

function viaHandler(event, functionName) {  
  const handler = require(`../../functions/api/${functionName}`).handler;
  log.debug(`invoking via handler function ${functionName}`);

  return new Promise((resolve, reject) => {
    const context = {};
    const callback = function (err, response) {  
      if (err) {
        reject(err);
      } else {
        // if the response is JSON, then make the test code simpler by parsing
        // the JSON body and return the parsed JSON object instead
        let contentType = _.get(response, 'headers.content-type', 'application/json');
        if (response.body && contentType === 'application/json') {
          response.body = JSON.parse(response.body);
        }

        resolve(response);
      }
    };

    handler(event, context, callback);
  });
}

const user_starts_stream = co.wrap(function* (userId, streamId) {
  const body = JSON.stringify({ streamId });
  const pathParameters = { userId };
  const headers = { 'Content-Type': 'application/json' };
  const event = { 
    body, 
    headers,
    pathParameters
  };

  const res = 
    mode === 'handler'
      ? yield viaHandler(event, 'start-stream')
      : yield viaHttp(`user/${userId || ''}/streams`, 'POST', { body, headers });

  return res;
});

const user_stops_stream = co.wrap(function* (userId, streamId) {
  const pathParameters = { userId, streamId };
  const event = { pathParameters };

  const res = 
    mode === 'handler'
      ? yield viaHandler(event, 'stop-stream')
      : yield viaHttp(`user/${userId || ''}/stream/${streamId}`, 'DELETE');

  return res;
});

module.exports = {
  user_starts_stream,
  user_stops_stream
};