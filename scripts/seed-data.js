'use strict';

const _           = require('lodash');
const Promise     = require('bluebird');
const co          = require('co');
const AWS         = require('aws-sdk');
AWS.config.region = 'eu-west-1';
const dynamodb    = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
const constants   = require('../lib/constants');
const userTable   = constants.USER_TABLE_NAME;
const streamTable = constants.STREAM_TABLE_NAME;
const given       = require('../tests/steps/given');

// create 10 random users & streams
_.range(0, 10)
  .forEach(co.wrap(function* () {
    yield given.a_random_user();
    yield given.a_random_stream();
  }));