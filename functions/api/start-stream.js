'use strict';

const co           = require('co');
const users        = require('../../lib/users');
const streams      = require('../../lib/streams');
const userStreams  = require('../../lib/userStreams');
const middy        = require('middy');
const errorHandler = require('../../lib/middlewares/errorHandling');
const captureCtx   = require('../../lib/middlewares/captureHttpContext');
const { warmup, httpHeaderNormalizer, jsonBodyParser, validator } = require('middy/middlewares');

const inputSchema = {
  required: [ 'body', 'pathParameters' ],
  properties: {
    body: {
      type: 'object',
      properties: {
        streamId: {
          type: 'string',
          minLength: 1,
          maxLength: 36
        }
      },
      required: [ 'streamId' ],
      additionalProperties: false
    },
    pathParameters: {
      type: 'object',
      properties: {
        userId: { 
          type: 'string',
          minLength: 1,
          maxLength: 36
        }
      },
      required: [ 'userId' ]
    }
  }
};

const outputSchema = {
  required: [ 'statusCode' ],
  properties: {
    statusCode: {
      type: 'integer'
    }
  },
  additionalProperties: false
}

const startStream = co.wrap(function* (event, context, cb) {
  const userId = event.pathParameters.userId;
  const streamId = event.body.streamId;

  yield users.must_exist(userId);
  yield streams.must_exist(streamId);

  yield userStreams.activate(userId, streamId);

  const resp = { statusCode: 200 };
  cb(null, resp);
});

const handler = middy(startStream)
  .use(warmup())
  .use(httpHeaderNormalizer())
  .use(jsonBodyParser())
  .use(validator({ inputSchema, outputSchema }))
  .use(captureCtx)
  .use(errorHandler);

module.exports = { handler };