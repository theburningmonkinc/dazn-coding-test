'use strict';

const co           = require('co');
const userStreams  = require('../../lib//userStreams');
const middy        = require('middy');
const errorHandler = require('../../lib/middlewares/errorHandling');
const { warmup, validator } = require('middy/middlewares');

const inputSchema = {
  required: [ 'pathParameters' ],
  properties: {
    pathParameters: {
      type: 'object',
      properties: {
        userId: { 
          type: 'string',
          minLength: 1,
          maxLength: 36
        },
        streamId: {
          type: 'string',
          minLength: 1,
          maxLength: 36
        }
      },
      required: [ 'userId', 'streamId' ]
    }
  }
};

const outputSchema = {
  required: [ 'statusCode' ],
  properties: {
    statusCode: {
      type: 'integer'
    }
  },
  additionalProperties: false
}

const stopStream = co.wrap(function* (event, context, cb) {  
  const userId = event.pathParameters.userId;
  const streamId = event.pathParameters.streamId;

  yield userStreams.deactivate(userId, streamId);

  const resp = { statusCode: 200 };
  cb(null, resp);
});

const handler = middy(stopStream)
  .use(warmup())
  .use(validator({ inputSchema, outputSchema }))
  .use(errorHandler);

module.exports = { handler };