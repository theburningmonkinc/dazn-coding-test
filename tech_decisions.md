# technical decisions

## use [Serverless](http://serverless.com) framework

One of the most popular and mature deployment framework available. I have a lot of experience using it, and it provides an abstraction layer that lets you be very productive, whilst also allows for great extensibility with its plugin system when you need to tailor its behaviour - e.g. we're using plugins in this project to enable X-Ray tracing, and per-function IAM policies.

## why install the Serverless framework as a local dependency to the project?

This allows anyone else to be able to build & deploy your functions without having to first install a compatible version of the Serverless framework on their machine (or on the CI server).

It also protects you against breaking changes in the framework, as it essentially locks this project with the version of the Serverless framwork it was developed with.

## including user ID in the URL

Ideally, I'd prefer: 

* `POST https://domain.com/me/streams` to start
* `DELETE https://domain.com/me/stream/{streamId}` to stop

because you need authentication either way (otherwise, it's possible to me to impersonate and start stream on another user's account), and this removes the ambiguity of `"what if the authentication resolves to a different user ID than the one in the path?"`.

But, this would require me to make decision about authentication (Cognito, Auth0, etc.), which seems out of scope for this coding test.

So, settled for:

* `POST https://domain.com/user/{userId}/streams` to start
* `DELETE https://domain.com/user/{userId}/stream/{streamId}` to stop

## authentication & authorization?

Again, leaving this off unless I have time to implement authentcation (or at least make a decision about it).

## including DynamoDB as CloudFormation resource

I know, I know, I said [you shouldn't include resources that store user information as part of your serverless.yml](https://hackernoon.com/aws-lambda-how-best-to-manage-shared-code-and-shared-infrastructure-827bed395eb7) because they can be accidentally wiped as you run `sls remove`.

But this is a code challenge with well-defined start & end state, and I **DO** want the reviewer to wipe everything from their account at the end.

## why DynamoDB?

Because it's a super scalable solution, with minimal management overhead. It now also has:

* auto-scaling out of the box, or you could use something like [FittedCloud](https://www.fittedcloud.com/blog/machine-learning-driven-optimization-outperforms-auto-scaling-dynamodb-capacity-management/) as well
* granular access control via IAM policies
* burst capacity to help deal with sudden spikes
* on-demand backup and restore
* continous backup and restore coming in near future

The simple (and very constrained) schema and the lack of transactional support can be a deal-breaker, but you can often design around them.

The scaling model (by read & write throughput units) is very constraining, but I see that as a positive as it forces you to think about how you'll scale your database ops early on. Too often, the ease of use (e.g. MongoDB) lures developers into a false sense of security and they discover nasty scaling issues.

Oh, and its cost scales linearly with actual usage, and even at scale (say, we were doing 10k writes/s in one of the games at Gamesys) the performance was still very stable and the cost was a tiny fraction of the revenue that you should expect to generate at that scale.

## user-streams table doesn't store `streamid` as range key

After a quick consideration, a more natural way to model this would be:

* HASH KEY : `userid`
* RANGE KEY : `streamid`

But, since we don't have transactions with DynamoDB (not without some elaborate patterns such as the [Saga pattern](https://read.acloud.guru/how-the-saga-pattern-manages-failures-with-aws-lambda-and-step-functions-bc8f7129f900)), it'll mean we have a race condition on our hands.
i.e. 2 concurrent requests to start, both count (via a DynamoDB `query` op) 2 active streams, and each add a new stream.

So, the easiest schema I can think that allows me to meet the requirements without resorting to implementing transactions with DynamoDB would be:

* one row for each user, hash key `userid`
* active streams are recorded as a `Set`, which allows to atomically insert/delete with conditional writes

Alternatively, maybe DynamoDB is just not the right data base option for this? Possibly, but as explained in the above point, it does meet the criteria that we have right now.

## what, no unit tests?

As I have explained in a [post](https://hackernoon.com/yubls-road-to-serverless-part-2-testing-and-ci-cd-72b2e583fe64) before, the risk profile for serverless applications have changed:

* the code inside the function has become drastically simpler
* most of what can go wrong is now outside of your function - configurations, permissions, etc.

As such, return-on-investment of unit tests do not justify its position as the workhorse of our test suite anymore. Instead, integration tests that checks the integration points with external services and how our code interacts with them. 

This shift in the risk profile of our systems holds true for most microservices architectures too.
[Many](https://labs.spotify.com/2018/01/11/testing-of-microservices/) [others](https://medium.com/@copyconstruct/testing-microservices-the-sane-way-9bb31d158c16) in this space have reached a similar conclusion, and I like the term `"testing honeycomb"` that describes this approach where integration tests make up the bulk of our test suite.

At small scale, end-to-end acceptance tests (tests your system from the outside, without calling into its internal code, e.g. calling the deployed API via its HTTP endpoint) still deliver a lot of value:

* it detects permission related problems, e.g. function is missing permission to perform `dynamodb:UpdateItem`
* it detects misconfigurations, e.g. function is configured with the wrong HTTP path

However, at scale and when the micro-services in your system become more interdependent and complex, these acceptance tests and their inherent dependency on your downstream being correct - i.e. service B, C, D and E all depend on service A, so if service A has a bug then all the other services' acceptance tests would fail too, which creates a lot of noise and can potentially block those other services from progressing through the CD pipeline.

At this point, you might be better off considering something along the lines of consumer-driven contracts for pre-production testing (and perhaps using frameworks such as [PACT](https://docs.pact.io/) to orchestrate them), and swift more of your focus towards improving observability of your stack and conducting production "testing" (A/B testing, canary testing, chaos experiments, penetration testing, etc.). Netflix is perhaps an extreme example of this, but many of the Netflix engineers such as Ben Christensen has openly stated that at their scale, testing almost become useless and it doesn't pick up most of the things that can actually go wrong in production.

## why does the output schema validation disallow `additionalProperties`?

Because security is as much as about what our system should do as what it **SHOULDN'T** do, i.e. it shouldn't accidentally leak sensitive data via responses.

So if we know what is the expected result from the endpoints, then we should make sure no additional data are returned, either unintentionally or as a result of an exploit.

## why pack the aws-sdk into the deployment? doesn't it make the function bigger?

Yes, but it protects you from AWS Lambda updating the version of AWS SDK that's bundled into the execution environment without you knowing.

A few people have been stun by this a while back, and AWS recommends it as a best practice nowadays. It does make your deployment package bigger though.